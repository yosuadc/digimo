<!DOCTYPE html>
<html lang="en">
<?php require 'partials/head.php'; 
    $_SESSION['identifyDeletedData'] = 'roles';
?>
<title>Role | DIGIMO</title>
<body class="skin-blue-dark fixed-layout mini-sidebar">
    <?php require 'partials/loader.php'; ?>
    <div id="main-wrapper">
        <?php require 'partials/header.php'; ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Role</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home.php">Home</a></li>
                                <li class="breadcrumb-item active">Role</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tabel Role</h4>
                                    <div class="float-md-right">
                                        <div class="button-box">
                                            <button type="button" 
                                                class="btn btn-info d-none float-right d-lg-block mr-4" 
                                                data-toggle="modal" 
                                                data-target="#modalCrud" 
                                                data-whatever="modalCrud"><i class="fa fa-plus-circle"></i> Tambah Data
                                            </button>
                                            <a href="deleted-data.php" 
                                                class="btn btn-warning d-none float-right d-lg-block mr-4"
                                                ><i class="fa fa-trash"></i> Recycle
                                            </a>
                                        </div>
                                        <div class="modal fade" id="modalCrud" tabindex="-1" role="dialog" aria-labelledby="modalMastering">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content" style="background-color: #353c48">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="judul">Tambah Role</h4>
                                                    </div>
                                                    <div class="modal-body" style="background-color: #353c48">
                                                        <form id="add-role" method="POST">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label>Nama Role</label>
                                                                    <input type="hidden" class="form-control" name="method" value="store" readonly="">
                                                                    <input type="hidden" class="form-control" name="id"readonly="">
                                                                    <input type="text" class="form-control" name="name" placeholder="nama hak akses" pattern="[A-Z a-z 0-9]+" required="">
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" form="add-role" class="btn btn-success saveButton"><i class="fa fa-check"></i> Save</button>
                                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="modal-notif" tabindex="-1" role="dialog" aria-labelledby="modalMastering">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content" style="background-color: #353c48">
                                                    <div class="modal-body" style="background-color: #353c48">
                                                        <h1 id="notif"></h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="table-responsive m-t-10">
                                    <?php
                                        $selectRoles = $mysqli->executeQuery('SELECT * FROM roles WHERE deleted_at IS NULL ORDER BY name');
                                        $roles = $selectRoles->getData();   
                                    ?>
                                    <table id="example23"
                                        class="display compact wrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Option</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php $i = 1; foreach($roles as $role) : ?>
                                                <tr>
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $role['name']; ?></td>
                                                    <td>
                                                        <div class="buttonContainer">
                                                            <button class="btn btn-success buttonInline" data-toggle="modal" data-target="#modalCrud"data-method="update" data-id="<?=$role['id']?>" data-name="<?=$role['name']?>"><span class="fa fa-edit"></span>
                                                            </button>
                                                            <button class="btn btn-danger buttonInline" data-method="delete" data-id="<?=$role['id']?>" data-name="<?=$role['name']?>"><span class="fa fa-trash"></span>
                                                            </button>
                                                        </div>    
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                        </div>
                    </div>
                </div>
                <?php require 'partials/sidebar.php'; ?>
            </div>
        </div>
        <?php require 'partials/footer.php'; ?>
    </div>
    <?php require 'partials/foot.php'; ?>
    <!-- This is data table -->
    <script src="assets/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="dist/js/datatable/buttons.min.js"></script>
    <script src="dist/js/datatable/flash.min.js"></script>
    <script src="dist/js/datatable/html5.min.js"></script>
    <script src="dist/js/datatable/print.min.js"></script>
    <script src="dist/js/datatable/jszip.min.js"></script>
    <script src="dist/js/datatable/pdfmake.min.js"></script>
    <script src="dist/js/datatable/vfs_fonts.js"></script>
    <script>
        $(function () {
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'excel', 'pdf'
                ]
            });
            $('.buttons-pdf, .buttons-excel').addClass('btn waves-effect waves-light btn-secondary ml-4');
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#modalCrud').on('show.bs.modal', function(e) {
                let method = $(e.relatedTarget).data('method');
                let id = $(e.relatedTarget).data('id');
                let name = $(e.relatedTarget).data('name');
                if (method == 'update') { 
                    $(e.currentTarget).find('input[name="method"]').val(method);
                    $(e.currentTarget).find('input[name="id"]').val(id); 
                    $(e.currentTarget).find('input[name="name"]').val(name).removeAttr("readonly");
                    $("#form-reason").hide();
                    $("#judul").html("Edit Data Role");
                }else{       
                  $(e.currentTarget).find('input[name="method"]').val('store');        
                    $(e.currentTarget).find('input[name="id"]').val('');
                    $(e.currentTarget).find('input[name="name"]').val('').removeAttr("readonly");
                    $("#form-reason").hide();
                    $("#judul").html("Tambah Data Role");
                }
            });

            $("#add-role").submit(function( event ) {
                let method = $(event.currentTarget).find('input[name="method"]').val();
                let msg = '';
                (method == 'update') ? msg = 'diperbaharui' : msg = 'ditambahkan';
                let datastring = $("#add-role").serialize();
                event.preventDefault();

                $.ajax({ 
                    type: "POST", 
                    url: "roles-process.php", 
                    data: {dataForm : datastring},
                }).then((result) => {
                    if(result == '1'){
                        Swal.fire("Berhasil!", "data berhasil " + msg, "success"
                        ).then(() => {
                            window.location.href = "roles.php";
                        });
                    }else {
                        Swal.fire("Error!", result, "error");
                    }
                });
            });

            $('.btn-danger').on('click', function(e) {
                let id = $(this).data('id');
                let name = $(this).data('name');
                Swal.fire({
                    title: "Hapus Data",
                    text: "Hapus sementara data " + name + " ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "Ya, Hapus",
                    closeOnConfirm: false
                }).then((result) => {
                    if (result.value) {
                        $.ajax({ 
                            type: "POST", 
                            url: "roles-process.php", 
                            data: {dataForm : "method=delete&id="+id}
                        }).then((result) => {
                            if(result == '1'){
                                Swal.fire('Berhasil!', 'Data ' + name + ' berhasil dihapus sementara', 'success'
                                ).then(() => {
                                    window.location.href = "roles.php";
                                });
                            }else {
                                Swal.fire('Gagal!', result,'error');
                            }
                        });
                        
                    }
                });
            });
        });
    </script>
</body>
</html>