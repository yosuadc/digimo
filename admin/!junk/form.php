<!DOCTYPE html>
<html lang="en">
<?php require 'partials/head.php'; ?>
<title>Forms | CFUMC</title>
<body class="skin-blue-dark fixed-layout">
    <?php require 'partials/loader.php'; ?>
    <div id="main-wrapper">
        <?php require 'partials/header.php'; ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Basic Form</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Basic Form</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Input States</h4>
                            <h5 class="card-subtitle"> Validation styles for error, warning, and success states on form controls.</h5>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <form class="form-horizontal row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="inputSuccess1">Input with success</label>
                                                <input type="text" class="form-control is-valid" id="inputSuccess1">
                                                <div class="valid-feedback">
                                                    Success! You've done it.
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-control-label" for="inputDanger1">Input with danger</label>
                                                <input type="text" class="form-control is-invalid" id="inputDanger1">
                                                <div class="invalid-feedback">
                                                    Sorry, that username's taken. Try another?
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <form class="form-horizontal">
                                        <div class="form-group row">
                                            <label for="inputHorizontalSuccess" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control is-valid" id="inputHorizontalSuccess" placeholder="name@example.com">
                                                <div class="valid-feedback">Success! You've done it.</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputHorizontalDnger" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control is-invalid" id="inputHorizontalDnger" placeholder="name@example.com">
                                                <div class="invalid-feedback">Sorry, that username's taken. Try another?</div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-5 offset-sm-1 col-xs-12">
                                    <form class="form-horizontal row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="col-sm-3 form-control-label" for="example-input-small">Small</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="example-input-small" name="example-input-small" class="form-control form-control-sm" placeholder="form-control-sm">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 form-control-label" for="example-input-normal">Normal</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="example-input-normal" name="example-input-normal" class="form-control" placeholder="Normal">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 form-control-label" for="example-input-large">Large</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="example-input-large" name="example-input-large" class="form-control form-control-lg" placeholder="form-control-lg">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 form-control-label">Grid Sizes</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" placeholder=".col-4">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-5 col-sm-offset-3">
                                                    <input type="text" class="form-control" placeholder=".col-5">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card card-body">
                            <h4 class="card-title">Sample Basic Forms</h4>
                            <h5 class="card-subtitle"> Bootstrap Elements </h5>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail111">User Name</label>
                                            <input type="text" class="form-control" id="exampleInputEmail111" placeholder="Enter Username">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail12">Email address</label>
                                            <input type="email" class="form-control" id="exampleInputEmail12" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword11">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword11" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword12">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword12" placeholder="Confirm Password">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" id="checkbox1" value="check">
                                                <label class="custom-control-label" for="checkbox1">Remember Me</label>
                                            </div>
                                        </div>
                                        <div class="form-group row pt-3">
                                            <div class="col-sm-4">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">Check this custom checkbox</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                    <label class="custom-control-label" for="customCheck2">Check this custom checkbox</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                                                    <label class="custom-control-label" for="customCheck3">Check this custom checkbox</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio2">Toggle this custom radio</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                          <label class="custom-control-label" for="customSwitch1">Toggle this switch element</label>
                                        </div>
                                        <div class="mt-5">
                                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                                            <button type="submit" class="btn btn-dark">Cancel</button>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require 'partials/sidebar.php'; ?>
            </div>
        </div>
        <?php require 'partials/footer.php'; ?>
    </div>
    <script src="../assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/node_modules/popper/popper.min.js"></script>
    <script src="../assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="../assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
</body>
</html>