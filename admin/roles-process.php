<?php 

require 'partials/database.php';
$mysqli = new Database;
$mysqli = $mysqli->dbConnect();

parse_str($_POST['dataForm'], $datas);
foreach($datas as $key => $value) {
	${$key} = htmlspecialchars($value, ENT_QUOTES);
}
$date = gmdate("Y-m-d H:i:s", time() + 3600*7);

switch ($method) {
	case 'store':
		$stmt = $mysqli->prepare("INSERT INTO roles (name) VALUES (?)");
		if (
			$stmt &&
			$stmt->bind_param('s', $name) &&
			$stmt->execute() &&
			$stmt->affected_rows === 1
		){
			echo json_encode(1);
		}else {
			echo json_encode($stmt->error);
		}
		$stmt->close();
		$mysqli->close();
		break;
	case 'update':
		$stmt = $mysqli->prepare("UPDATE roles SET name = ? WHERE id = ?");
		if (
			$stmt &&
			$stmt->bind_param('si', $name, $id) &&
			$stmt->execute() &&
			$stmt->affected_rows === 1
		) {
			echo json_encode(1);
		} else {
			echo json_encode($stmt->error);
		}
		$stmt->close();
		$mysqli->close();
		break;
	case 'delete':
		$stmt = $mysqli->prepare("UPDATE roles SET deleted_at = ? WHERE id = ?");
		if (
			$stmt &&
			$stmt->bind_param('si', $date, $id) &&
			$stmt->execute() &&
			$stmt->affected_rows === 1
		) {
			echo json_encode(1);
		} else {
			echo json_encode($stmt->error);
		}
		$stmt->close();
		$mysqli->close();
		break;
	case 'restore':
		$stmt = $mysqli->prepare("UPDATE roles SET deleted_at = NULL WHERE id = ?");
		if (
			$stmt &&
			$stmt->bind_param('i', $id) &&
			$stmt->execute() &&
			$stmt->affected_rows === 1
		) {
			echo json_encode(1);
		} else {
			echo json_encode($stmt->error);
		}
		$stmt->close();
		$mysqli->close();
		break;
	case 'forceDelete':
		$stmt = $mysqli->prepare("DELETE FROM roles WHERE id = ?");
		if (
			$stmt &&
			$stmt->bind_param('i', $id) &&
			$stmt->execute() &&
			$stmt->affected_rows === 1
		) {
			echo json_encode(1);
		} else {
			echo json_encode($stmt->error);
		}
		$stmt->close();
		$mysqli->close();
		break;
	default:
		echo json_encode('Method Not Found');
		break;
}
?>