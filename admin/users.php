<!DOCTYPE html>
<html lang="en">
<?php require 'partials/head.php'; 
    $_SESSION['identifyDeletedData'] = 'users';
?>
<title>User | DIGIMO</title>
<body class="skin-blue-dark fixed-layout mini-sidebar">
    <?php require 'partials/loader.php'; ?>
    <div id="main-wrapper">
        <?php require 'partials/header.php'; ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">User</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home.php">Home</a></li>
                                <li class="breadcrumb-item active">User</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <?php
                    $selectRoles = $mysqli->executeQuery('SELECT * FROM roles WHERE deleted_at IS NULL ORDER BY name');
                    $roles = $selectRoles->getData();
                    $selectUsers = $mysqli->executeQuery('SELECT * FROM users WHERE deleted_at IS NULL ORDER BY email');
                    $users = $selectUsers->getData();    
                ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tabel User</h4>
                                    <div class="float-md-right">
                                        <div class="button-box">
                                            <button type="button" 
                                                class="btn btn-info d-none float-right d-lg-block mr-4" 
                                                data-toggle="modal" 
                                                data-target="#modalCrud" 
                                                data-whatever="modalCrud"><i class="fa fa-plus-circle"></i> Tambah Data
                                            </button>
                                            <a href="deleted-data.php" 
                                                class="btn btn-warning d-none float-right d-lg-block mr-4"
                                                ><i class="fa fa-trash"></i> Recycle
                                            </a>
                                        </div>
                                        <div class="modal fade" id="modalCrud" tabindex="-1" role="dialog" aria-labelledby="modalMastering">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content" style="background-color: #353c48">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="judul">Tambah User</h4>
                                                    </div>
                                                    <div class="modal-body" style="background-color: #353c48">
                                                        <form id="add-role" method="POST">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <input type="hidden" name="mode" value="store" readonly="">
                                                                    <input type="hidden" name="id"readonly="">
                                                                    <label>Email User</label>
                                                                    <input type="text" class="form-control" name="email" placeholder="user@mail.com" pattern="[A-Z a-z 0-9 @]+" required="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Hak Ases</label>
                                                                    <select name="role_id" id="selectRoles" class="form-control">
                                                                        <?php foreach ($roles as $role) : ?>
                                                                            <option value="<?= $role['id']; ?>"><?= $role['name']; ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Password</label>
                                                                    <input type="text" class="form-control" name="password" required="">
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" form="add-role" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="modal-notif" tabindex="-1" role="dialog" aria-labelledby="modalMastering">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content" style="background-color: #353c48">
                                                    <div class="modal-body" style="background-color: #353c48">
                                                        <h1 id="notif"></h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="table-responsive m-t-10">
                                    <table id="example23"
                                        class="display compact wrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Email</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No.</th>
                                                <th>Email</th>
                                                <th>Option</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php $i = 1; foreach($users as $user) : ?>
                                                <tr>
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $user['email']; ?></td>
                                                    <td>
                                                        <div class="buttonContainer">
                                                            <button class="btn btn-success buttonInline" data-toggle="modal" data-target="#modalCrud"data-mode="update" data-id="<?=$user['id']?>" data-name="<?=$user['name']?>"><span class="fa fa-edit"></span>
                                                            </button>
                                                            <button class="btn btn-danger buttonInline" data-toggle="modal" data-target="#modalCrud" data-mode="delete" data-id="<?=$user['id']?>" data-name="<?=$user['name']?>"><span class="fa fa-trash"></span>
                                                            </button>
                                                        </div>    
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                        </div>
                    </div>
                </div>
                <?php require 'partials/sidebar.php'; ?>
            </div>
        </div>
        <?php require 'partials/footer.php'; ?>
    </div>
    <?php require 'partials/foot.php'; ?>
    <!-- This is data table -->
    <script src="assets/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="dist/js/datatable/buttons.min.js"></script>
    <script src="dist/js/datatable/flash.min.js"></script>
    <script src="dist/js/datatable/html5.min.js"></script>
    <script src="dist/js/datatable/print.min.js"></script>
    <script src="dist/js/datatable/jszip.min.js"></script>
    <script src="dist/js/datatable/pdfmake.min.js"></script>
    <script src="dist/js/datatable/vfs_fonts.js"></script>
    <script>
        $(function () {
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'excel', 'pdf'
                ]
            });
            $('.buttons-pdf, .buttons-excel').addClass('btn waves-effect waves-light btn-secondary ml-4');
        });
    </script>
    <script>
        $(document).ready(function() {
        
            $('#modalCrud').on('show.bs.modal', function(e) {
                var mode = $(e.relatedTarget).data('mode');
                var id = $(e.relatedTarget).data('id');
                var email = $(e.relatedTarget).data('email');
                var role_id = $(e.relatedTarget).data('role_id');
                var password = $(e.relatedTarget).data('password');
                if (mode == 'update') { 
                    $(e.currentTarget).find('input[name="mode"]').val(mode);
                    $(e.currentTarget).find('input[name="id"]').val(id); 
                    $(e.currentTarget).find('input[name="email"]').val(email).removeAttr("readonly");
                    $(e.currentTarget).find('input[name="role_id"]').val(role_id).removeAttr("readonly");
                    $(e.currentTarget).find('input[name="password"]').val(password).removeAttr("readonly");
                    $("#form-reason").hide();
                    $("#judul").html("Edit Data User");
                }
                else if (mode == 'delete') {
                    $(e.currentTarget).find('input[name="mode"]').val(mode).attr("readonly","");  
                    $(e.currentTarget).find('input[name="id"]').val(id); 
                    $(e.currentTarget).find('input[name="email"]').val(email).removeAttr("readonly");
                    $(e.currentTarget).find('input[name="role_id"]').val(role_id).removeAttr("readonly");
                    $(e.currentTarget).find('input[name="password"]').val(password).removeAttr("readonly");
                    $("#form-reason").show();
                    $("#judul").html("Apakah anda ingin <u>menghapus</u> data ini ?");
                }else{       
                  $(e.currentTarget).find('input[name="mode"]').val('store');        
                    $(e.currentTarget).find('input[name="id"]').val('');
                    $(e.currentTarget).find('input[name="name"]').val('').removeAttr("readonly");
                    $("#form-reason").hide();
                    $("#judul").html("Tambah Data User");
                }
            });

            $("#add-role").submit(function( event ) {
                var datastring = $("#add-role").serialize();
                event.preventDefault();

                $.ajax({
                    type: "POST",
                    url: "users-process.php",
                    data: {dataForm : datastring},
                    success: function(result){
                        $('#modalCrud').modal('toggle');
                        $('#modal-notif').modal('toggle');
                        $('#notif').html(result);
                        setTimeout(function() {
                          window.location.href = "users.php";
                        }, 1000);
                    }
                });
            });
        });
    </script>
</body>
</html>