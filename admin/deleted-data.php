<!DOCTYPE html>
<html lang="en">
<?php require 'partials/head.php';
    $deletedDatas = $_SESSION['identifyDeletedData'];
?>
<title>Recycle Bin | CFUMC</title>
<body class="skin-blue-dark fixed-layout mini-sidebar">
    <?php require 'partials/loader.php'; ?>
    <div id="main-wrapper">
        <?php require 'partials/header.php'; ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Deleted <?= $deletedDatas ?></h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home.php">Home</a></li>
                                <li class="breadcrumb-item">Recycle Bin</li>
                                <li class="breadcrumb-item active"><?= $deletedDatas ?></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Table <?= $deletedDatas ?></h4>
                                <a href="home.php" 
                                    class="btn btn-info d-none float-right d-lg-block mr-4"
                                    ><i class="fa fa-home"></i> Home
                                </a>
                                <a href="<?=$deletedDatas?>.php" 
                                    class="btn btn-primary d-none float-right d-lg-block mr-4"
                                    ><i class="fa fa-arrow-left"></i> Back
                                </a>
                                <div class="table-responsive m-t-10">
                                    <?php
                                        $selectDatas = $mysqli->executeQuery("SELECT * FROM $deletedDatas WHERE deleted_at IS NOT NULL");
                                        $datas = $selectDatas->getData();        
                                    ?>
                                    <table id="example23"
                                        class="display compact wrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Deleted At</th>
                                                <!-- <th>Reason</th> -->
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Deleted At</th>
                                                <!-- <th>Reason</th> -->
                                                <th>Option</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php $i = 1; foreach($datas as $data) : ?>
                                                <tr>
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $data['name'] ?? ''; ?></td>
                                                    <td><?= $data['deleted_at'] ?? ''; ?></td>
                                                    <td>
                                                        <div class="buttonContainer">
                                                            <button class="btn btn-warning buttonInline" 
                                                                data-method="restore" 
                                                                data-id="<?=$data['id']?>"
                                                                data-name="<?=$data['name']?>"><span class="fa fa-undo"></span>
                                                            </button>
                                                            <button class="btn btn-danger buttonInline" 
                                                                data-method="forceDelete" 
                                                                data-id="<?=$data['id']?>"
                                                                data-name="<?=$data['name']?>"><span class="fa fa-trash"></span>
                                                            </button>
                                                        </div>    
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                        </div>
                    </div>
                </div>
                <?php require 'partials/sidebar.php'; ?>
            </div>
        </div>
        <?php require 'partials/footer.php'; ?>
    </div>
    <?php require 'partials/foot.php'; ?>
    <!-- This is data table -->
    <script src="assets/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script>
        $(function () {
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'excel', 'pdf'
                ]
            });
            $('.buttons-pdf, .buttons-excel').addClass('btn waves-effect waves-light btn-secondary ml-4');
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.btn-danger').on('click', function(e) {
                var id = $(this).data('id');
                var name = $(this).data('name');
                Swal.fire({
                    title: "Hapus data secara PERMANEN",
                    text: "apakah anda yakin menghapus data " + name,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya, Hapus",
                    closeOnConfirm: false
                }).then((result) => {
                    if (result.value) {
                        $.ajax({ 
                            type: "POST", 
                            url: "<?=$deletedDatas?>-process.php", 
                            data: {dataForm : "method=forceDelete&id="+id}
                        }).then((result) => {
                            if(result == '1'){
                                Swal.fire('Berhasil!', 'Data ' + name + ' berhasil dihapus secara permanen', 'success'
                                ).then(() => {
                                    window.location.href = "deleted-data.php";
                                });
                            }else {
                                Swal.fire('Gagal!', result,'error');
                            }
                        });
                        
                    }
                });
            });
            $('.btn-warning').on('click', function(e) {
                var id = $(this).data('id');
                var name = $(this).data('name');
                Swal.fire({
                    title: "Restore Data",
                    text: "apakah anda yakin mengembalikan data dengan detail: " + name,
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#FEC107",
                    confirmButtonText: "Ya, Lanjutkan",
                    closeOnConfirm: false
                }).then((result) => {
                    if (result.value) {
                        $.ajax({ 
                            type: "POST", 
                            url: "<?=$deletedDatas?>-process.php", 
                            data: {dataForm : "method=restore&id="+id}
                        }).then((result) => {
                            if(result == '1'){
                                Swal.fire('Berhasil!', 'Data ' + name + ' berhasil dikembalikan', 'success'
                                ).then(() => {
                                    window.location.href = "deleted-data.php";
                                });
                            }else {
                                Swal.fire('Gagal!', result,'error');
                            }
                        });
                        
                    }
                });
            });
        });
    </script>
</body>
</html>