<!DOCTYPE html>
<html lang="en">
<?php require 'partials/head.php'; 
    $_SESSION['identifyDeletedData'] = 'programs';
?>
<link href="assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
<title>Daftar Program | CFUMC</title>
<body class="skin-blue-dark fixed-layout mini-sidebar">
    <?php require 'partials/loader.php'; ?>
    <div id="main-wrapper">
        <?php require 'partials/header.php'; ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Program</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home.php">Home</a></li>
                                <li class="breadcrumb-item active">Program</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tabel Program</h4>
                                    <div class="float-md-right">
                                        <div class="button-box">
                                            <button type="button" 
                                                class="btn btn-info d-none float-right d-lg-block mr-4" 
                                                data-toggle="modal" 
                                                data-target="#modalCrud" 
                                                data-whatever="modalCrud"><i class="fa fa-plus-circle"></i> Tambah Data
                                            </button>
                                            <a href="deleted-data.php" 
                                                class="btn btn-warning d-none float-right d-lg-block mr-4"
                                                ><i class="fa fa-trash"></i> Recycle
                                            </a>
                                        </div>
                                        <div class="modal fade" id="modalCrud" tabindex="-1" role="dialog" aria-labelledby="modalMastering">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content" style="background-color: #353c48">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="judul">Tambah Program</h4>
                                                    </div>
                                                    <div class="modal-body" style="background-color: #353c48">
                                                        <form id="add-organization" method="POST">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label>Nama Program</label>
                                                                    <input type="hidden" class="form-control" name="mode" value="store" readonly="">
                                                                    <input type="hidden" class="form-control" name="id"readonly="">
                                                                    <input type="text" class="form-control" name="name" placeholder="awali dengan huruf kapital (UKM ... / HMP ...)" pattern="[A-Z a-z]+" required="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Organisasi</label>
                                                                    <select class="select2 form-control custom-select" style="width: 100%; height:auto;">
                                                                        <option>Pilih Organisasi</option>
                                                                        <option value="AK">HMP 1</option>
                                                                        <option value="HI">HMP 2</option>
                                                                        <option value="CA">UKM 1</option>
                                                                        <option value="NV">UKM 2</option>
                                                                    </select>
                                                                </div>                                                                
                                                                <div id="form-reason">
                                                                    <div class="form-group">
                                                                        <label>Keterangan</label>
                                                                        <input type="text" class="form-control" name="deleted_reason" placeholder="alasan menghapus.." pattern="[A-Z a-z]+" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" form="add-organization" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="modal-notif" tabindex="-1" role="dialog" aria-labelledby="modalMastering">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content" style="background-color: #353c48">
                                                    <div class="modal-body" style="background-color: #353c48">
                                                        <h1 id="notif"></h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="table-responsive m-t-10">
                                    <?php
                                        $selectPrograms = $mysqli->executeQuery('
                                            SELECT programs.id, organizations.name as organization, programs.name, programs.isUProgram
                                            FROM programs INNER JOIN organizations
                                            ON programs.organizations_id = organizations.id
                                            WHERE programs.deleted_at IS NULL AND organizations.deleted_at IS NULL
                                            ORDER BY programs.name ASC
                                        ');
                                        $programs = $selectPrograms->getData();   
                                    ?>
                                    <table id="example23"
                                        class="display compact wrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Program</th>
                                                <th>Lingkup</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Program</th>
                                                <th>Lingkup</th>
                                                <th>Option</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php $i = 1; foreach($programs as $program) : ?>
                                                <tr>
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $program['name']; ?></td>
                                                    <td><?= $program['organization']; ?></td>
                                                    <td><?php echo (($program['isUProgram'] == 1) ? "Universitas" : "Fakultas"); ?></td>
                                                    <td>
                                                        <div class="buttonContainer">
                                                            <button class="btn btn-success buttonInline" data-toggle="modal" data-target="#modalCrud"data-mode="update" data-id="<?=$program['id']?>" data-name="<?=$program['name']?>"><span class="fa fa-edit"></span>
                                                            </button>
                                                            <button class="btn btn-danger buttonInline" data-toggle="modal" data-target="#modalCrud" data-mode="delete" data-id="<?=$program['id']?>" data-name="<?=$program['name']?>"><span class="fa fa-trash"></span>
                                                            </button>
                                                        </div>    
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                        </div>
                    </div>
                </div>
                <?php require 'partials/sidebar.php'; ?>
            </div>
        </div>
        <?php require 'partials/footer.php'; ?>
    </div>
    <?php require 'partials/foot.php'; ?>
    <!-- This is data table -->
    <script src="assets/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="dist/js/datatable/buttons.min.js"></script>
    <script src="dist/js/datatable/flash.min.js"></script>
    <script src="dist/js/datatable/html5.min.js"></script>
    <script src="dist/js/datatable/print.min.js"></script>
    <script src="dist/js/datatable/pdfmake.min.js"></script>
    <script src="dist/js/datatable/jszip.min.js"></script>
    <script src="dist/js/datatable/vfs_fonts.js"></script>

    <script src="assets/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script>
        $(function () {
            $(".select2").select2();

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'excel', 'pdf'
                ]
            });
            $('.buttons-pdf, .buttons-excel').addClass('btn waves-effect waves-light btn-secondary ml-4');
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        
            $(".ajax").select2({
                ajax: {
                    url: "https://api.github.com/search/repositories",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                //templateResult: formatRepo, // omitted for brevity, see the source of this page
                //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });

            $.fn.modal.Constructor.prototype._enforceFocus = function() {};

            $('#modalCrud').on('show.bs.modal', function(e) {
                var mode = $(e.relatedTarget).data('mode');
                var id = $(e.relatedTarget).data('id');
                var name = $(e.relatedTarget).data('name');
                var deleted_reason = $(e.relatedTarget).data('deleted_reason');
                if (mode == 'update') { 
                    $(e.currentTarget).find('input[name="mode"]').val(mode);
                    $(e.currentTarget).find('input[name="id"]').val(id); 
                    $(e.currentTarget).find('input[name="name"]').val(name).removeAttr("readonly");
                    $(e.currentTarget).find('input[name="deleted_reason"]').val(deleted_reason).removeAttr("required","");
                    $("#form-reason").hide();
                    $("#judul").html("Edit Data Program");
                }
                else if (mode == 'delete') {
                    $(e.currentTarget).find('input[name="mode"]').val(mode).attr("readonly","");  
                    $(e.currentTarget).find('input[name="id"]').val(id); 
                    $(e.currentTarget).find('input[name="name"]').val(name).attr("readonly","");
                    $("#form-reason").show();
                    $(e.currentTarget).find('input[name="deleted_reason"]').val(deleted_reason).attr("required","");
                    $("#judul").html("Apakah anda ingin <u>menghapus</u> data ini ?");
                }else{       
                  $(e.currentTarget).find('input[name="mode"]').val('store');        
                    $(e.currentTarget).find('input[name="id"]').val('');
                    $(e.currentTarget).find('input[name="name"]').val('').removeAttr("readonly");
                    $(e.currentTarget).find('input[name="deleted_reason"]').val(deleted_reason).removeAttr("required","");
                    $("#form-reason").hide();
                    $("#judul").html("Tambah Data Program");
                }
            });

            $("#add-organization").submit(function( event ) {
                var datastring = $("#add-organization").serialize();
                event.preventDefault();

                $.ajax({ /* THEN THE AJAX CALL */
                    type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "programs-process.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {dataForm : datastring}, /* THE DATA WE WILL BE PASSING */
                    success: function(result){ /* GET THE TO BE RETURNED DATA */
                        $('#modalCrud').modal('toggle');
                        $('#modal-notif').modal('toggle');
                        $('#notif').html(result);
                        setTimeout(function() {
                          window.location.href = "organisasi.php";
                        }, 1000);
                    }
                });
            });
        });
    </script>
</body>
</html>