<?php

	function dd($variable){
		return die(var_dump($variable));
	}

	function redirect($url){
		echo "<script language='javascript'>window.location.href='".$url."'</script>";
	}

	function escape($value){
		$value = trim($value);
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		$value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
		$value = strip_tags($value);
		$value = mysqli_real_escape_string($value);
		$value = htmlspecialchars($value);
		return $value;
	}

?>