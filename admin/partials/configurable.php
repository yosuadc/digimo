<?php

if(preg_match("/configurable.php/", $_SERVER['PHP_SELF'])){
	header("Location: ../index.php");
	die;
}

trait Configurable {
	abstract protected function getConfig();

	protected function setProperties() {
		$config = $this->getConfig();

		foreach ($config as $key => $config) {
			$this->{$key} = $config;
		}
	}
}