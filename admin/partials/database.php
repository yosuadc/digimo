<?php

if(preg_match("/database.php/", $_SERVER['PHP_SELF'])){
	header("Location: ../index.php");
	die;
}

require 'partials/config.php';
require 'partials/configurable.php';

class Database {
	use Configurable;

	public $db, $host, $user, $password, $dbname, $result, $stmt, $data = [], $params = [];

	public function __construct()
	{
		$this->setProperties();

		$this->db = new mysqli($this->host, $this->user, $this->password, $this->dbname);

		if($this->db->connect_error) {
			die(var_dump($this->db->connect_error));
		}

		return $this->db;
	}

	public function doesntExist() {
		return $this->result->num_rows < 1;
	}

	public function dbConnect(){
		if ($this->db instanceof mysqli) {
            return $this->db;
       }
	}

	public function executeQuery(string $queryString) {
		if ($this->db->prepare($queryString)) {
			$this->result = $this->db->query($queryString);
			return $this;
		}

		$this->handleError();
	}

	public function exists() {
		return $this->result->num_rows > 0;
	}

	protected function getConfig() {
		return Config::mysql();
	}

	public function getData() {
		$data = [];

		while ($row = $this->result->fetch_assoc()) {
			$data[] = $row;
		}

		$this->result->free();

		// $this->db->close();

		return $data;
	}

	protected function handleError() {
		if ($this->db->error) {
			// die(var_dump($this->db->error));
			echo $this->db->error;
		}
	}

	public function prepare(string $sql) {
		$this->stmt = $this->db->prepare($sql);

		if (!$this->stmt) {die($this->db->error);}

		return $this;
	}

	public function query(string $sql) {
		$this->stmt = $this->db->prepare($sql);

		if (!$this->stmt) {var_dump($this->db->error);}

		return $this;
	}

	public function work() {
		$this->stmt->execute();

		return $this;
	}
}
