<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Committe Fair - Web-Based Work Porgram Registration">
    <meta name="author" content="https://www.instagram.com/yosuadc">
    <link rel="icon" type="image/png" sizes="16x16" href="dist/images/logo.png">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="dist/css/pages/stylish-tooltip.css" rel="stylesheet">
    <link href="assets/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="assets/toast-master/css/jquery.toast.css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="assets/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="assets/datatables.net-bs4/css/responsive.dataTables.min.css">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
</head>
<style type="text/css">
    .buttonContainer{
        width:100%;
        text-align: center;
    }
    .buttonInline{
        display: inline-block;
    }

    .blinker {
      animation: blinker 0.75s linear infinite;
    }
    @keyframes blinker {  
      50% { opacity: 0; }
    }

    .blinker:hover {
      opacity: 1;
      -webkit-animation-name: none;
    }


</style>
<?php
    session_start();
    require 'partials/database.php';
    require 'partials/functions.php';
    $mysqli = new Database;
    date_default_timezone_set("ASIA/JAKARTA");
?>