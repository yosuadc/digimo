<?php 

require 'partials/database.php';
$mysqli = new Database;
$mysqli = $mysqli->dbConnect();

parse_str($_POST['dataForm'], $data);
$method = htmlspecialchars($data['mode'], ENT_QUOTES);
$id = htmlspecialchars($data['id'], ENT_QUOTES);
$name = htmlspecialchars($data['name'], ENT_QUOTES);
$date = gmdate("Y-m-d H:i:s", time() + 3600*7);
$date = htmlspecialchars($date, ENT_QUOTES);

switch ($method) {
	case 'store':
		$stmt = $mysqli->prepare("INSERT INTO roles (name) VALUES (?)");
		if (
			$stmt &&
			$stmt->bind_param('s', $name) &&
			$stmt->execute() &&
			$stmt->affected_rows === 1
		){
			echo "Data $name berhasil dibuat";
		}else {
			echo ($stmt->errno === 1062 ? "Data $name sudah ada !!" : "undefined errno $stmt->errno");
		}
		$stmt->close();
		$mysqli->close();
		break;
	case 'update':
		$stmt = $mysqli->prepare("UPDATE organizations SET name = ? WHERE id = ?");
		if (
			$stmt &&
			$stmt->bind_param('si', $name, $id) &&
			$stmt->execute() &&
			$stmt->affected_rows === 1
		) {
			echo "Berhasil memperbarui data $name";
		} else {
			echo ($stmt->errno === 1062 ? "Data $name sudah ada !!" : "undefined errno $stmt->errno");
		}
		$stmt->close();
		$mysqli->close();
		break;
	case 'delete':
		$stmt = $mysqli->prepare("UPDATE roles SET deleted_at = ? WHERE id = ?");
		$stmt->bind_param('si', $date, $id);
		$stmt->execute();
		$stmt->close();
		$mysqli->close();
		break;
	case 'restore':
		$stmt = $mysqli->prepare("UPDATE organizations SET deleted_at = NULL, deleted_reason = NULL WHERE id = ?");
		$stmt->bind_param('i', $id);
		$stmt->execute();
		$stmt->close();
		$mysqli->close();
		break;
	default:
		echo "403 FORBIDDEN";
		break;
}
?>