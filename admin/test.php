<!DOCTYPE html>
<html lang="en">
<?php require 'partials/head.php'; 
    $_SESSION['identifyDeletedData'] = 'programs';
?>
<link href="assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
<title>Daftar Program | CFUMC</title>
<body class="skin-blue-dark fixed-layout mini-sidebar">
    <?php require 'partials/loader.php'; ?>
    <div id="main-wrapper">
        <?php require 'partials/header.php'; ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Program</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home.php">Home</a></li>
                                <li class="breadcrumb-item active">Program</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    $stmt = $mysqli->executeQuery("SELECT * FROM organizations WHERE deleted_at is NUll ORDER BY name ASC ");
                                    $organizations = $stmt->getData();

                                    // $stmt->close();
                                    // $mysqli->close();

                                    $dropdown= '';
                                    foreach ($organizations as $organization) {
                                        $id = $organization['id'];
                                        $name = $organization['name'];
                                        $dropdown .= "<option value=\"$id\">$name</option>";
                                    }
                                ?>

                                <h4 class="card-title">Select 2</h4>
                                <h6 class="card-subtitle"> Select2 for custom search and select</h6>
                                <h5 class="m-t-30">Single select2</h5>
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;">
                                    <option>Select Organisasi</option>
                                    <?= $dropdown; ?>
                                </select>
                                <h5 class="m-t-20">Multiple select boxes</h5>
                                <select class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose">
                                    <optgroup label="Alaskan/Hawaiian Time Zone">
                                        <option value="AK">Alaska</option>
                                        <option value="HI">Hawaii</option>
                                    </optgroup>
                                    <optgroup label="Pacific Time Zone">
                                        <option value="CA">California</option>
                                        <option value="NV">Nevada</option>
                                        <option value="OR">Oregon</option>
                                        <option value="WA">Washington</option>
                                    </optgroup>
                                    <optgroup label="Mountain Time Zone">
                                        <option value="AZ">Arizona</option>
                                        <option value="CO">Colorado</option>
                                        <option value="ID">Idaho</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="UT">Utah</option>
                                        <option value="WY">Wyoming</option>
                                    </optgroup>
                                    <optgroup label="Central Time Zone">
                                        <option value="AL">Alabama</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TX">Texas</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="WI">Wisconsin</option>
                                    </optgroup>
                                    <optgroup label="Eastern Time Zone">
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="IN">Indiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="OH">Ohio</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WV">West Virginia</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="card">
                        </div>
                    </div>
                </div>
                <?php require 'partials/sidebar.php'; ?>
            </div>
        </div>
        <?php require 'partials/footer.php'; ?>
    </div>
    <?php require 'partials/foot.php'; ?>
    <!-- This is data table -->
    <script src="assets/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

    <script src="assets/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script>
        $(function () {
            $(".select2").select2();

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'excel', 'pdf'
                ]
            });
            $('.buttons-pdf, .buttons-excel').addClass('btn waves-effect waves-light btn-secondary ml-4');
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        
            $(".ajax").select2({
                ajax: {
                    url: "https://api.github.com/search/repositories",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                //templateResult: formatRepo, // omitted for brevity, see the source of this page
                //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });

            $('#modalCrud').on('show.bs.modal', function(e) {
                var mode = $(e.relatedTarget).data('mode');
                var id = $(e.relatedTarget).data('id');
                var name = $(e.relatedTarget).data('name');
                var deleted_reason = $(e.relatedTarget).data('deleted_reason');
                if (mode == 'update') { 
                    $(e.currentTarget).find('input[name="mode"]').val(mode);
                    $(e.currentTarget).find('input[name="id"]').val(id); 
                    $(e.currentTarget).find('input[name="name"]').val(name).removeAttr("readonly");
                    $(e.currentTarget).find('input[name="deleted_reason"]').val(deleted_reason).removeAttr("required","");
                    $("#form-reason").hide();
                    $("#judul").html("Edit Data Program");
                }
                else if (mode == 'delete') {
                    $(e.currentTarget).find('input[name="mode"]').val(mode).attr("readonly","");  
                    $(e.currentTarget).find('input[name="id"]').val(id); 
                    $(e.currentTarget).find('input[name="name"]').val(name).attr("readonly","");
                    $("#form-reason").show();
                    $(e.currentTarget).find('input[name="deleted_reason"]').val(deleted_reason).attr("required","");
                    $("#judul").html("Apakah anda ingin <u>menghapus</u> data ini ?");
                }else{       
                  $(e.currentTarget).find('input[name="mode"]').val('store');        
                    $(e.currentTarget).find('input[name="id"]').val('');
                    $(e.currentTarget).find('input[name="name"]').val('').removeAttr("readonly");
                    $(e.currentTarget).find('input[name="deleted_reason"]').val(deleted_reason).removeAttr("required","");
                    $("#form-reason").hide();
                    $("#judul").html("Tambah Data Program");
                }
            });

            $("#add-organization").submit(function( event ) {
                var datastring = $("#add-organization").serialize();
                event.preventDefault();

                $.ajax({ /* THEN THE AJAX CALL */
                    type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                    url: "programs-process.php", /* PAGE WHERE WE WILL PASS THE DATA */
                    data: {dataForm : datastring}, /* THE DATA WE WILL BE PASSING */
                    success: function(result){ /* GET THE TO BE RETURNED DATA */
                        $('#modalCrud').modal('toggle');
                        $('#modal-notif').modal('toggle');
                        $('#notif').html(result);
                        setTimeout(function() {
                          window.location.href = "organisasi.php";
                        }, 1000);
                    }
                });
            });
        });
    </script>
</body>
</html>