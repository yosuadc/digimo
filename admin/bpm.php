<!DOCTYPE html>
<html lang="en">
<?php require 'partials/head.php'; ?>
<link href="dist/css/pages/login-register-lock.css" rel="stylesheet">
<title>Welcome | CFUMC</title>
<body class="skin-default card-no-border">
    <?php require 'partials/loader.php'; ?>
    <section id="wrapper">
        <div class="login-register">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" action="index.html">
                        <h3 class="text-center m-b-20">Sign In</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="Username" pattern="[A-Z a-z]+" minlength="4" maxlength="10"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Password" minlength="6" maxlength="30"> </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?php require 'partials/foot.php'; ?>
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
</body>
</html>